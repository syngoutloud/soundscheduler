﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class handleCurvedLine : MonoBehaviour
{

    public GameObject lineStart;
    public GameObject lineEnd;
    public GameObject lineStartHiddenDraggable;
    public GameObject lineEndHiddenDraggable;
    public LineRenderer line;
    public GameObject roomCenter;

    [Range(1, 1.97f)]
    public float curveRadius = 1.97f;

    float lineWidth = 0.13f;
    float angleStart;
    float angleEnd;
    float angle;
    float arcLength;
    List<Vector2> arcPoints = new List<Vector2>();
    int numberOfSegments;
    public Color lineColor;

    // Start is called before the first frame update
    void Start()
    {
        
        numberOfSegments = 60;
        line.positionCount = numberOfSegments;
        line.startWidth = lineWidth;
        line.endWidth = lineWidth;
        line.material.color = lineColor;

        setCurve();
        getHandlePositions();
        drawArc();

    }

    

Vector2 GetPointOnCircle(Vector2 origin, float radius, float angle)
    {

        float angleInRadians = angle * Mathf.Deg2Rad;

        var x = origin.x + radius * Mathf.Sin(angleInRadians);
        var y = origin.y + radius * Mathf.Cos(angleInRadians);

        return new Vector2(x, y);

    }

    // Update is called once per frame
    void Update()
    {
        if (lineStartHiddenDraggable.GetComponent<handleHandleGestures>().isPressed == true || lineEndHiddenDraggable.GetComponent<handleHandleGestures>().isPressed == true)
        {

            setCurve();

        }
 

    }

    void setCurve()
    {
        //START
        //get direction from touch to room center
        Vector3 vectorToLineStart = lineStartHiddenDraggable.transform.position - roomCenter.transform.position;

        //get its angle
        angleStart = -(Mathf.Atan2(vectorToLineStart.y, vectorToLineStart.x) * Mathf.Rad2Deg - 90);

        //position visible handle at angle/fixed radius around a circle
        lineStart.transform.position = GetPointOnCircle(new Vector2(roomCenter.transform.position.x, roomCenter.transform.position.y), curveRadius, angleStart);

        //rotate handle too
        lineStart.transform.eulerAngles = new Vector3(0, 0, -angleStart);

        //set start of line to handle pos
        line.SetPosition(0, lineStart.transform.position);


        //END
        //get direction from touch to room center
        Vector3 vectorToLineEnd = lineEndHiddenDraggable.transform.position - roomCenter.transform.position;

        //get its angle
        angleEnd = -(Mathf.Atan2(vectorToLineEnd.y, vectorToLineEnd.x) * Mathf.Rad2Deg - 90);

        //position visible handle at angle/fixed radius around a circle
        lineEnd.transform.position = GetPointOnCircle(new Vector2(roomCenter.transform.position.x, roomCenter.transform.position.y), curveRadius, angleEnd);

        //rotate handle too
        lineEnd.transform.eulerAngles = new Vector3(0, 0, -angleEnd);

        //set start of line to handle pos
        line.SetPosition(line.positionCount - 1, lineEnd.transform.position);


        getHandlePositions();
        drawArc();

    }

    void getHandlePositions()
    {

        //get direction from touch to room center
        Vector3 vectorToLineStart = lineStart.transform.position - roomCenter.transform.position;

        //get its angle
        angleStart = -(Mathf.Atan2(vectorToLineStart.y, vectorToLineStart.x) * Mathf.Rad2Deg) + 90;

        //get direction from touch to room center
        Vector3 vectorToLineEnd = lineEnd.transform.position - roomCenter.transform.position;

        //get its angle
        angleEnd = -(Mathf.Atan2(vectorToLineEnd.y, vectorToLineEnd.x) * Mathf.Rad2Deg) + 90;

    }

    void drawArc()
    {
        //kill any arc already there
        arcPoints.Clear();

        //avoid minus angles

        if (angleEnd < 0)
        {
            angleEnd += 360;
        }

        if (angleStart < 0)
        {
            angleStart += 360;
        }

        //set start point
        angle = angleStart;

        //set length
        arcLength = angleEnd - angleStart;


        //make sure the line is always drawn clockwise from start to end
        float arcLengthConverted;

        if (arcLength < 0)
        {
            arcLengthConverted = 360 + arcLength;
        }
        else
        {
            arcLengthConverted = arcLength;
        }


        //draw!
        for (int i = 0; i <= numberOfSegments-1; i++)
        {
            float x = roomCenter.transform.position.x + Mathf.Sin(Mathf.Deg2Rad * angle) * curveRadius;
            float y = roomCenter.transform.position.y + Mathf.Cos(Mathf.Deg2Rad * angle) * curveRadius;

            arcPoints.Add(new Vector2(x, y));

            angle += (arcLengthConverted / numberOfSegments);
           
            line.SetPosition(i, arcPoints[i]);

        }

        

    }

    
}
