﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shapes2D;

public class handleRingSegment : MonoBehaviour
{
    public GameObject targetObject;
    public GameObject clockCenter;
    float angle;
    public int soundDurationHours;
    public int soundDurationResolution;
    public int durationDegrees;
    public int degreesPerHour;
    public GameObject segmentCenterParent;


    // Start is called before the first frame update
    void Start()
    {
        degreesPerHour = 15;
        soundDurationResolution = 2; //i.e. half hours
        soundDurationHours = 3;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (targetObject.GetComponent<handleObject>().isPressed == true)
        {
            rotate();
        }
        
        setDuration();
    }

    void rotate()
    {
        Vector3 vectorToTarget = targetObject.transform.position - clockCenter.transform.position;
        //Debug.DrawLine(targetObject.transform.position, clockCenter.transform.position, Color.green, Time.deltaTime, false);
        angle = -(Mathf.Atan2(vectorToTarget.x, vectorToTarget.y) * Mathf.Rad2Deg) + 90 + (durationDegrees/2);
        transform.eulerAngles = new Vector3(0, 0, snappedAngle(angle, 360, (24 * soundDurationResolution))); //i.e. 48 = snap to half hours
        //print(angle);

        segmentCenterParent.transform.localEulerAngles = new Vector3(0, 0, (degreesPerHour * soundDurationHours) / 2);
    }

    void setDuration()
    {

        durationDegrees = soundDurationHours * (30 / soundDurationResolution);

        GetComponent<Shape>().settings.startAngle = 0;

        GetComponent<Shape>().settings.endAngle = durationDegrees;

    }

    int snappedAngle(float original, int numerator, int denominator)
    {
        return Mathf.RoundToInt(original * denominator / numerator) * numerator / denominator;
    }
}
