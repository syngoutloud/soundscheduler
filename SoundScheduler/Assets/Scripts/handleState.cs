﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;
using DG.Tweening;

public class handleState : MonoBehaviour
{
    string currentTime;
    string prevTime;

    int currentSecond;
    int secondsSinceMidnight;
    int currentMinute;
    int currentHour;
    int currentTimeZoneHourOffset;
    int daylightSaving; //true/false = 1/0

    public int debugFakeHourOffset = 0;

    public GameObject clockPlayhead;

    float anglePerSecSinceMidnight = 0.004186046512f;

    TimeZoneInfo localZone;

    public int intHours;
    public string labelHours;
    public string labelMinutes;
    public string labelSeconds;
    public TextMeshPro labelTotalTime;

    // Start is called before the first frame update
    void Start()
    {
        localZone = TimeZoneInfo.Local;
        prevTime = "0";
        currentSecond = 0;
    }

    // Update is called once per frame
    void Update()
    {
        checkTime();
    }

    void checkTime()
    {
        currentTime = System.DateTime.UtcNow.ToString();

        currentSecond = System.DateTime.UtcNow.Second;

        currentMinute = System.DateTime.UtcNow.Minute;

        currentTimeZoneHourOffset = (int)localZone.BaseUtcOffset.TotalHours;

        if (localZone.IsDaylightSavingTime(DateTime.Now) == true) {
            daylightSaving = 1;
        } else {
            daylightSaving = 0;
        }

        currentHour = System.DateTime.UtcNow.Hour + currentTimeZoneHourOffset + daylightSaving + debugFakeHourOffset;

        secondsSinceMidnight = ((currentHour * 3600) + (currentMinute * 60)) + currentSecond;

        if (currentTime != prevTime)
        {

            if (currentHour < 10)
            {
                labelHours = "0" + currentHour.ToString();
            }
            else
            {
                labelHours = currentHour.ToString();
            }

            if (currentMinute < 10)
            {
                labelMinutes = "0" + currentMinute.ToString();
            }
            else
            {
                labelMinutes = currentMinute.ToString();
            }

            if (currentSecond < 10)
            {
                labelSeconds = "0" + currentSecond.ToString();
            }
            else
            {
                labelSeconds = currentSecond.ToString();
            }

            labelTotalTime.text = labelHours + "<voffset=0.1em>" + ":" + "<voffset=0em>" + labelMinutes + "<voffset=0.1em>" + ":" + "<voffset=0em>" + labelSeconds;

            clockPlayhead.transform.eulerAngles = new Vector3(0, 0, (-(secondsSinceMidnight * anglePerSecSinceMidnight)+180));

            if (currentHour > 11)
            {
                labelTotalTime.gameObject.transform.localEulerAngles = new Vector3(0, 0, 90);
               
            }
            else
            {
                labelTotalTime.gameObject.transform.localEulerAngles = new Vector3(0, 0, -90);
               
            }

            prevTime = currentTime;

        }
    }
}
