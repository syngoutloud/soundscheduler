﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using DG.Tweening;

public class handleHandleGestures : MonoBehaviour
{

	public GameObject target;
    public bool isPressed;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<ReleaseGesture>().StateChanged += releaseHandler;
        GetComponent<PressGesture>().StateChanged += pressHandler;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void releaseHandler(object sender, GestureStateChangeEventArgs e)
    {

        ReleaseGesture gesture = sender as ReleaseGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
            transform.DOMove(target.transform.position, 0);
            isPressed = false;
        }

    }

    private void pressHandler(object sender, GestureStateChangeEventArgs e)
    {

        PressGesture gesture = sender as PressGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
            print("ye");
            isPressed = true;
        }

    }

}
