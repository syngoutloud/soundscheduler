﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using DG.Tweening;

public class handleObject : MonoBehaviour
{

    public bool isPressed;
    public GameObject roomCenter;
    float distanceFromRoomCenter;
    float roomEdgeThreshold;
    public GameObject drawerTarget;

    // Start is called before the first frame update
    void Start()
    {
        isPressed = false;
        roomEdgeThreshold = 2;

        GetComponent<ReleaseGesture>().StateChanged += releaseHandler;
        GetComponent<PressGesture>().StateChanged += pressHandler;

        distanceFromRoomCenter = Vector3.Distance(gameObject.transform.position, roomCenter.transform.position);
        setPosition();
    }

    // Update is called once per frame
    void Update()
    {
        if (isPressed == true)
        {
            distanceFromRoomCenter = Vector3.Distance(gameObject.transform.position, roomCenter.transform.position);
            setPosition();
        }
        
    }

    private void pressHandler(object sender, GestureStateChangeEventArgs e)
    {

        PressGesture gesture = sender as PressGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
           // print("pressed");
            isPressed = true;
           // setObjectSegmentVisibility();
        }

    }

    private void releaseHandler(object sender, GestureStateChangeEventArgs e)
    {

        ReleaseGesture gesture = sender as ReleaseGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
            isPressed = false;
            setPosition();
        }

    }

    void setPosition()
    {

        
        if (distanceFromRoomCenter < (roomEdgeThreshold * 1.05f)) //if we're in the room...
        {

        }
        else
        {


            if (isPressed == false)
            {
                transform.DOMove(drawerTarget.transform.position, 0.3f).SetEase(Ease.OutBack);
            }

        }

        
    }

}
